#    CRIADO POR MARCOS VAZ   #
#     05/10/2017 - 01:08     #
#        VERSÃO - 0.3        #
# -------------------------- #
# OBSERVAÇÃO:                # 
# O CÓDIGO ESTÁ ESTRUTURADO  #
# DA SEGUINTE FORMA:         #
# - ORDEM DO HTML;           #
# - ORDEM ALFABÉTICA;        #
# -------------------------- #
#   www.mvdeveloper.com.br   #

<!-- HTML -->
# Cada página utiliza as FONTS,
# do FontAwesome; os CSS de grids
# responsivos; os CSS do
# Animate.css; a "CDN" do
# Normalize.css; a CDN do jQuery;
# e as Google Fonts.

<!-- CSS -->
# Arquivos CSS estão separados
# por elementos principais de
# cada página, que serão
# reutilizados em outras, e
# assim deixando o código de
# fácil manutenção.

<!-- JS -->
# O JavaScript está sendo usado
# com a biblioteca do jQuery,
# e foi dividido em Functions;
# Usei também validação com
# o jQuery Plugin Mask.

<!-- VERSÕES -->
# 0.3 -> Adicionado ENTRAR.html,
# PRODUTO.html, CARRINHO.html e
# respectivos CSS.

# 0.2 -> Estilizado completamente
# o INDEX.html e alterado nome
# para SHOP.html.

# 0.1 -> Começo do projeto,
# criação do INDEX.html e
# começo de sua estilização.